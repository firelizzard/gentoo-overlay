# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-group

DESCRIPTION="Group for the 1Password password manager"
ACCT_GROUP_ID=1010 # Needs to be higher than 1000, from previous issues.
