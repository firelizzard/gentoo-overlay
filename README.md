This is a portage overlay for my Gentoo ebuilds.

The 1Password ebuilds are largely copied from
https://github.com/jaredallard/overlay, I just added signature verification.